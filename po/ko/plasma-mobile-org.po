#
# Shinjo Park <kde@peremen.name>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: websites-plasma-mobile-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-18 00:44+0000\n"
"PO-Revision-Date: 2022-05-07 02:36+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: config.yaml:0 content/_index.md:0
msgid "Plasma Mobile"
msgstr "Plasma 모바일"

#: config.yaml:0
msgid "Privacy-respecting, open source and secure phone ecosystem"
msgstr "프라이버시를 존중하고 보안을 중시하는 오픈 소스 휴대폰 생태계"

#: config.yaml:0
msgid "Project"
msgstr "프로젝트"

#: config.yaml:0
msgid "Contributing"
msgstr "기여"

#: config.yaml:0
msgid "Documentation"
msgstr "문서"

#: config.yaml:0
msgid "Reporting Issues"
msgstr "문제점 보고"

#: content/about.md:0
msgid "About"
msgstr "정보"

#: content/about.md:9
msgid ""
"Plasma Mobile is an open-source user interface and ecosystem targeted at "
"mobile devices, built on the **KDE Plasma** stack."
msgstr ""
"Plasma 모바일은 **KDE Plasma** 스택을 사용하는 모바일 장치용 오픈 소스 사용"
"자 인터페이스이자 생태계입니다."

#: content/about.md:11
msgid ""
"A pragmatic approach is taken that is inclusive to software regardless of "
"toolkit, giving users the power to choose whichever software they want to "
"use on their device."
msgstr ""
"실용적인 접근 방식을 사용하여 툴킷에 관계 없이 소프트웨어를 포용하며, 사용자"
"가 원하는 대로 장치에 소프트웨어를 설치할 수 있습니다."

#: content/about.md:13
msgid ""
"Plasma Mobile aims to contribute to and implement open standards, and is "
"developed in a transparent process that is open for anyone to participate in."
msgstr ""
"Plasma 모바일은 공개 표준에 기여하고 구현하는 것을 목표로 하며, 누구나 참여"
"할 수 있는 투명한 과정으로 개발됩니다."

#: content/about.md:15
msgid "<img src=\"/img/konqi/konqi-calling.png\" width=250px/>\n"
msgstr "<img src=\"/img/konqi/konqi-calling.png\" width=250px/>\n"

#: content/about.md:19
msgid "Can I use it?"
msgstr "사용할 수 있나요?"

#: content/about.md:21
msgid ""
"Yes! Plasma Mobile is currently preinstalled on the PinePhone using Manjaro "
"ARM. There are also various other distributions for the PinePhone with "
"Plasma Mobile available."
msgstr ""
"네! Plasma 모바일은 PinePhone에 Manjaro ARM 위에 설치되어 있습니다. Plasma 모"
"바일이 탑재된 PinePhone용 다른 배포판도 있습니다."

#: content/about.md:23
msgid ""
"There is also postmarketOS, an Alpine Linux-based distribution with support "
"for many more devices and it offers Plasma Mobile as an available interface "
"for the devices it supports. You can see the list of supported devices [here]"
"(https://wiki.postmarketos.org/wiki/Devices), but on any device outside the "
"main and community categories your mileage may vary."
msgstr ""
"Alpine 리눅스 기반 배포판인 postmarketOS에서도 사용할 수 있습니다. 더 많은 모"
"바일 장치를 지원하며 지원하는 장치 인터페이스로 Plasma 모바일을 제공합니다. "
"지원하는 장치 목록은 [여기](https://wiki.postmarketos.org/wiki/Devices)에서 "
"확인할 수 있지만, 주 및 커뮤니티 분류 외에 있는 장치 지원은 안정적이지 않을 "
"수도 있습니다."

#: content/about.md:25
msgid ""
"The interface is using KWin over Wayland and is now mostly stable, albeit a "
"little rough around the edges in some areas. A subset of the normal KDE "
"Plasma features are available, including widgets and activities, both of "
"which are integrated into the Plasma Mobile UI. This makes it possible to "
"use and develop for Plasma Mobile on your desktop/laptop."
msgstr ""
"인터페이스는 KWin/Wayland를 사용하며 일부 다듬어지지 않은 부분을 제외하면 안"
"정적으로 사용 가능합니다. 위젯과 활동을 포함한 일반 KDE Plasma 기능의 일부분"
"을 사용할 수 있으며, Plasma 모바일 UI에 통합되어 있습니다. 즉 Plasma 모바일"
"을 데스크톱/노트북에서 사용하고 개발할 수 있습니다."

#: content/about.md:27
msgid "What can it do?"
msgstr "무엇을 할 수 있나요?"

#: content/about.md:29
msgid ""
"There are quite a few touch-optimized apps that are now being bundled with "
"the Manjaro-based Plasma Mobile image, allowing a wide range of basic "
"functions."
msgstr ""
"기본 기능을 수행할 수 있도록 Manjaro 기반 Plasma 모바일 이미지에 일부 터치 최"
"적화된 앱이 포함되어 있습니다."

#: content/about.md:31
msgid ""
"These are mostly built with Kirigami, KDE’s interface framework allowing "
"convergent UIs that work very well in a touch-only environment."
msgstr ""
"대부분은 KDE의 인터페이스 프레임워크인 Kirigami를 사용했습니다. 터치 전용 환"
"경에서도 작동하는 통합형 UI를 구현합니다."

#: content/about.md:33
msgid ""
"You can find a list of these applications on [plasma-mobile.org homepage]"
"(https://plasma-mobile.org)."
msgstr ""
"[plasma-mobile.org 홈페이지](https://plasma-mobile.org)에서 앱 목록을 확인할 "
"수 있습니다."

#: content/about.md:35
msgid "Where can I find…"
msgstr "어디에서 찾을 수 있나요?"

#: content/about.md:37
msgid ""
"The code for various Plasma Mobile components can be found on [invent.kde."
"org](https://invent.kde.org/plasma-mobile)."
msgstr ""
"다양한 Plasma 모바일 구성 요소의 소스 코드는 [invent.kde.org](https://invent."
"kde.org/plasma-mobile)에서 찾을 수 있습니다."

#: content/about.md:39
msgid ""
"Read the [contributors documentation](https://invent.kde.org/plasma/plasma-"
"mobile/-/wikis/home) for more details on the Plasma stack and how everything "
"fits together."
msgstr ""
"Plasma 스택에 대한 정보와 모든 것이 작동하는 방식을 알아 보려면 [기여자 문서]"
"(https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)를 읽어 보십시오."

#: content/about.md:41
msgid ""
"You can also ask your questions in the [Plasma Mobile community groups and "
"channels](/join)."
msgstr "[Plasma 모바일 커뮤니티 그룹과 채널](/join)에 질문할 수도 있습니다."

#: content/blog/_index.md:0
msgid "Blog"
msgstr "블로그"

#: content/get.md:0 i18n/en.yaml:0
msgid "Install"
msgstr "설치"

#: content/get.md:0
msgid "Distributions offering Plasma Mobile"
msgstr "Plasma 모바일이 탑재된 배포판"

#: content/get.md:13
msgid "Listed below are distributions that ship Plasma Mobile."
msgstr "아래는 Plasma 모바일이 탑재된 배포판 목록입니다."

#: content/get.md:15
msgid ""
"Please check the information for each distribution to see if your device is "
"supported."
msgstr "장치 지원 여부는 각각 배포판 문서를 확인하십시오."

#: content/get.md:17
msgid "Mobile"
msgstr "휴대폰"

#: content/get.md:19 content/get.md:128
msgid "postmarketOS"
msgstr "postmarketOS"

#: content/get.md:21 content/get.md:130
msgid "![](/img/pmOS.svg)"
msgstr "![](/img/pmOS.svg)"

#: content/get.md:23
msgid ""
"PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that "
"can be installed on smartphones and other mobile devices. View the [device "
"list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for "
"supporting your device."
msgstr ""
"PostmarketOS(pmOS)는 스마트폰과 기타 모바일 장치에 설치할 수 있는 터치에 최적"
"화된 Alpine 리눅스의 파생형입니다. 장치 지원 여부를 확인하려면 [장치 목록]"
"(https://wiki.postmarketos.org/wiki/Devices)을 참조하십시오."

#: content/get.md:25
msgid ""
"For devices that do not have prebuilt images, you will need to flash it "
"manually using the `pmbootstrap` utility. Follow instructions [here](https://"
"wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the "
"device's wiki page for more information on what is working."
msgstr ""
"미리 빌드된 이미지가 없는 장치를 사용하고 있다면 `pmbootstrap` 유틸리티를 사"
"용하여 수동으로 플래시해야 합니다. [여기](https://wiki.postmarketos.org/wiki/"
"Installation_guide)에 있는 안내 사항을 따르십시오. 진행하기 전에 장치 지원 상"
"태를 장치별 위키 페이지에서 확인하십시오."

#: content/get.md:27
msgid "[Learn more](https://postmarketos.org)"
msgstr "[더 알아보기](https://postmarketos.org)"

#: content/get.md:29 content/get.md:47
msgid "Download"
msgstr "다운로드"

#: content/get.md:31
msgid "[Well supported devices](https://postmarketos.org/download/)"
msgstr "[잘 지원되는 장치](https://postmarketos.org/download/)"

#: content/get.md:32
msgid "[Full Device List](https://wiki.postmarketos.org/wiki/Devices)"
msgstr "[전체 장치 목록](https://wiki.postmarketos.org/wiki/Devices)"

#: content/get.md:34
msgid "Nightly"
msgstr ""

#: content/get.md:35
msgid ""
"PostmarketOS provides a nightly repository that tracks **Plasma 6** "
"development."
msgstr ""

#: content/get.md:37
#, fuzzy
#| msgid "[Full Device List](https://wiki.postmarketos.org/wiki/Devices)"
msgid ""
"[Documentation](https://wiki.postmarketos.org/wiki/"
"Plasma_Mobile#Nightly_repository)"
msgstr "[전체 장치 목록](https://wiki.postmarketos.org/wiki/Devices)"

#: content/get.md:41
msgid "Arch Linux ARM"
msgstr "Arch Linux ARM"

#: content/get.md:43 content/get.md:140
msgid "![](/img/archlinux.png)"
msgstr "![](/img/archlinux.png)"

#: content/get.md:45
msgid ""
"Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX "
"community."
msgstr ""
"Arch Linux ARM은 DanctNIX 커뮤니티에 의해서 PinePhone과 PineTab으로 이식되었"
"습니다."

#: content/get.md:49
msgid "[Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)"
msgstr "[릴리스](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)"

#: content/get.md:53
msgid "Debian"
msgstr ""

#: content/get.md:55
#, fuzzy
#| msgid "![](/img/neon.svg)"
msgid "![](/img/debian.svg)"
msgstr "![](/img/neon.svg)"

#: content/get.md:57
msgid ""
"Debian provides a basic set of packages related to [plasma-mobile](packages."
"debian.org/plasma-mobile) in its repositories since release 12, codenamed "
"_bookworm_. The packaging effort is continuing even after the release of "
"bookworm, that means a broader set of packages is now available."
msgstr ""

#: content/get.md:59
msgid ""
"Thanks to the efforts of the developers of [Mobian](https://mobian-project."
"org/) project (a mobile derivative of Debian), flashable images based on "
"Plasma mobile will be soon available, as well!"
msgstr ""

#: content/get.md:61
#, fuzzy
#| msgid ""
#| "The code for various Plasma Mobile components can be found on [invent.kde."
#| "org](https://invent.kde.org/plasma-mobile)."
msgid ""
"Instructions to install Plasma mobile on Debian systems can be found on "
"[Debian wiki](https://wiki.debian.org/Mobian/Tweaks#Plasma_mobile)."
msgstr ""
"다양한 Plasma 모바일 구성 요소의 소스 코드는 [invent.kde.org](https://invent."
"kde.org/plasma-mobile)에서 찾을 수 있습니다."

#: content/get.md:65
msgid "Kupfer"
msgstr ""

#: content/get.md:67
#, fuzzy
#| msgid "![](/img/fedora.svg)"
msgid "![](/img/distros/kupfer.svg)"
msgstr "![](/img/fedora.svg)"

#: content/get.md:69
msgid ""
"Kupfer Linux is an Arch Linux ARM derived distribution aiming to provide "
"tooling that makes it easier for developers to port devices to it."
msgstr ""

#: content/get.md:71 content/get.md:85 content/get.md:97
msgid "Links"
msgstr ""

#: content/get.md:73
msgid "[Website](https://kupfer.gitlab.io/)"
msgstr ""

#: content/get.md:77
msgid "Sineware ProLinux"
msgstr ""

#: content/get.md:79
#, fuzzy
#| msgid "![](/img/archlinux.png)"
msgid "![](/img/distros/prolinux.png)"
msgstr "![](/img/archlinux.png)"

#: content/get.md:81
msgid ""
"ProLinux is a GNU/Linux distribution with an immutable root filesystem, an A/"
"B updating scheme, Flatpak-first app distribution model, and a unified API "
"surface to interact with the OS."
msgstr ""

#: content/get.md:83
msgid ""
"**Plasma 6** development images are currently available, built from git."
msgstr ""

#: content/get.md:86
msgid "[Website](https://sineware.ca/prolinux/)"
msgstr ""

#: content/get.md:87
msgid ""
"[Nightly Builds](http://cdn.sineware.ca/repo/prolinux/mobile/dev/arm64/)"
msgstr ""

#: content/get.md:91
msgid "openSUSE"
msgstr "openSUSE"

#: content/get.md:93
msgid "![](/img/openSUSE.svg)"
msgstr "![](/img/openSUSE.svg)"

#: content/get.md:95
msgid ""
"openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux "
"distribution sponsored by SUSE Linux GmbH and other companies. Currently "
"openSUSE provides Tumbleweed based Plasma Mobile builds."
msgstr ""
"openSUSE는 과거에 SUSE Linux, SuSE Linux Professional로도 알려져 있으며, "
"SUSE Linux GmbH 및 기타 회사에서 지원하는 리눅스 배포판입니다. 현재 openSUSE"
"에서는 Tumbleweed 기반 Plasma 모바일 빌드를 제공합니다."

#: content/get.md:98
#, fuzzy
#| msgid "[PinePhone](https://images.postmarketos.org/pinephone/)"
msgid "[Website](https://en.opensuse.org/HCL:PinePhone)"
msgstr "[PinePhone](https://images.postmarketos.org/pinephone/)"

#: content/get.md:99
msgid ""
"[PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/"
"Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64."
"raw.xz)"
msgstr ""
"[PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/"
"Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64."
"raw.xz)"

#: content/get.md:100
#, fuzzy
#| msgid ""
#| "[![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/"
#| "#plasmamobile:matrix.org)"
msgid "[Matrix Channel](https://matrix.to/#/#mobile:opensuse.org)"
msgstr ""
"[![](/img/matrix.svg)Matrix(가장 활성화됨)](https://matrix.to/#/"
"#plasmamobile:matrix.org)"

#: content/get.md:104 content/get.md:118
msgid "Fedora"
msgstr "페도라"

#: content/get.md:106 content/get.md:120
msgid "![](/img/fedora.svg)"
msgstr "![](/img/fedora.svg)"

#: content/get.md:108
msgid "This is a work in progress, stay tuned!"
msgstr "아직 작업 중이므로 계속 지켜 보세요!"

#: content/get.md:110
msgid ""
"Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:"
"fedoraproject.org) to get details on the progress."
msgstr ""
"진행 상황 정보는 페도라 Mobility [matrix 채널](https://matrix.to/#/#mobility:"
"fedoraproject.org)에서 확인하십시오."

#: content/get.md:112
msgid "Links to testing images can be found in the channel."
msgstr ""

#: content/get.md:116
msgid "Desktop Devices"
msgstr "데스크톱 장치"

#: content/get.md:122
msgid ""
"Fedora has Plasma Mobile and related applications packaged in their "
"repositories."
msgstr ""

#: content/get.md:124
msgid ""
"Install the [plasma-mobile](https://src.fedoraproject.org/rpms/plasma-"
"mobile) package."
msgstr ""

#: content/get.md:132
msgid ""
"postmarketOS is able to be run in QEMU, and thus is a suitable option for "
"trying Plasma Mobile on your computer."
msgstr ""
"postmarketOS는 QEMU에서 실행시킬 수 있으며, 컴퓨터에서 Plasma 모바일을 체험"
"해 볼 수 있는 좋은 방법입니다."

#: content/get.md:134
msgid ""
"Read more about it [here](https://wiki.postmarketos.org/wiki/"
"QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma "
"Mobile as the desktop environment."
msgstr ""
"더 많은 정보는 [여기](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-"
"amd64))에서 확인할 수 있습니다. 설치 과정에서 데스크톱 환경으로 Plasma 모바일"
"을 선택하십시오."

#: content/get.md:138
msgid "Arch Linux"
msgstr "Arch Linux"

#: content/get.md:142
#, fuzzy
#| msgid ""
#| "Plasma Mobile is now available on the [AUR](https://aur.archlinux.org/"
#| "packages/plasma-mobile)."
msgid ""
"Plasma Mobile is available on the [AUR](https://aur.archlinux.org/packages/"
"plasma-mobile)."
msgstr ""
"Plasma 모바일은 [AUR](https://aur.archlinux.org/packages/plasma-mobile)에서"
"도 사용할 수 있습니다."

#: content/get.md:146
msgid "KDE Neon"
msgstr ""

#: content/get.md:148
msgid "![](/img/neon.svg)"
msgstr "![](/img/neon.svg)"

#: content/get.md:150
msgid "**WARNING**: This is not actively maintained!"
msgstr "**경고**: 더 이상 활발하게 관리되지 않습니다!"

#: content/get.md:152
msgid ""
"This image, based on KDE neon, can be tested on non-android intel tablets, "
"PCs and virtual machines."
msgstr ""
"이 이미지는 KDE neon 기반이며 안드로이드를 사용하지 않는 인텔/AMD 태블릿, "
"PC, 가상 머신에서 실행 가능합니다."

#: content/get.md:154
msgid "[Neon amd64](https://files.kde.org/neon/images/mobile/)"
msgstr "[Neon amd64](https://files.kde.org/neon/images/mobile/)"

#: content/join.md:0
msgid "Community"
msgstr "커뮤니티"

#: content/join.md:12
msgid ""
"If you'd like to contribute to the amazing free software for mobile devices, "
"[join us - we always have a task for you](https://invent.kde.org/plasma/"
"plasma-mobile/-/wikis/home)!"
msgstr ""
"모바일 장치에 탑재되는 자유 소프트웨어에 기여하고 싶으시다면 [지금 참여하세"
"요! 여러분의 작업은 항상 도움이 됩니다!](https://invent.kde.org/plasma/"
"plasma-mobile/-/wikis/home)"

#: content/join.md:14
msgid "<img src=\"/img/konqi/konqi-contribute.png\" width=200px/>\n"
msgstr "<img src=\"/img/konqi/konqi-contribute.png\" width=200px/>\n"

#: content/join.md:18
msgid "Plasma Mobile community groups and channels:"
msgstr "Plasma 모바일 커뮤니티 그룹과 채널:"

#: content/join.md:20
msgid "Plasma Mobile specific channels:"
msgstr "Plasma 모바일 전용 채널:"

#: content/join.md:22
msgid ""
"[![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:"
"matrix.org)"
msgstr ""
"[![](/img/matrix.svg)Matrix(가장 활성화됨)](https://matrix.to/#/"
"#plasmamobile:matrix.org)"

#: content/join.md:24
msgid "[![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)"
msgstr "[![](/img/telegram.svg)텔레그램](https://t.me/plasmamobile)"

#: content/join.md:26
msgid ""
"[![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/"
"listinfo/plasma-mobile)"
msgstr ""
"[![](/img/mail.svg)Plasma 모바일 메일링 리스트](https://mail.kde.org/mailman/"
"listinfo/plasma-mobile)"

#: content/join.md:29
msgid "Plasma Mobile related project channels:"
msgstr "Plasma 모바일 관련 프로젝트 채널:"

#: content/join.md:31
msgid ""
"[![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/"
"mailman/listinfo/plasma-devel)"
msgstr ""
"[![](/img/mail.svg)Plasma 개발 메일링 리스트](https://mail.kde.org/mailman/"
"listinfo/plasma-devel)"

#: content/screenshots.md:0
msgid "Screenshots"
msgstr "스크린샷"

#: content/screenshots.md:0
msgid "Homescreen"
msgstr "홈 화면"

#: content/screenshots.md:0
msgid "App Drawer (Grid)"
msgstr "앱 서랍(격자)"

#: content/screenshots.md:0
msgid "App Drawer (List)"
msgstr "앱 서랍(목록)"

#: content/screenshots.md:0
msgid "Action Drawer (Expanded)"
msgstr "동작 서랍(확장됨)"

#: content/screenshots.md:0
msgid "Action Drawer (Minimized)"
msgstr "동작 서랍(최소화됨)"

#: content/screenshots.md:0
msgid "Task Switcher"
msgstr "작업 전환기"

#: content/screenshots.md:0
msgid "Weather"
msgstr "날씨"

#: content/screenshots.md:0
msgid "Calculator"
msgstr "계산기"

#: content/screenshots.md:0
msgid "Calendar"
msgstr "캘린더"

#: content/screenshots.md:0
msgid "Clock"
msgstr "시계"

#: content/screenshots.md:0
msgid "Angelfish, a web browser"
msgstr "Angelfish, 웹 브라우저"

#: content/screenshots.md:0
msgid "Discover, an app store"
msgstr "Discover, 앱 스토어"

#: content/screenshots.md:0
msgid "Plasma mobile homescreen"
msgstr "Plasma 모바일 홈 화면"

#: content/screenshots.md:0
msgid "Index, a file manager"
msgstr "Index, 파일 관리자"

#: content/screenshots.md:0
msgid "Koko, a photo library viewer"
msgstr "Koko, 사진 라이브러리 뷰어"

#: content/screenshots.md:0
msgid "Dialer"
msgstr "다이얼"

#: content/screenshots.md:0
msgid "Settings"
msgstr "설정"

#: content/screenshots.md:0
msgid "Elisa, a music player"
msgstr "Elisa, 음악 재생기"

#: content/screenshots.md:0
msgid "Megapixels, a camera application"
msgstr "Megapixels, 카메라 앱"

#: content/screenshots.md:0
msgid "The hardware"
msgstr "하드웨어"

#: content/screenshots.md:51
msgid ""
"The following screenshots were taken from a Pinephone device running Plasma "
"Mobile (April 2022)."
msgstr ""
"다음 스크린샷은 PinePhone에서 실행 중인 Plasma 모바일에서 촬영했습니다(2022"
"년 4월)."

#: i18n/en.yaml:0
msgid "Plasma, in your pocket."
msgstr "내 주머니 속의 Plasma."

#: i18n/en.yaml:0
msgid "Open up."
msgstr "열어 보세요."

#: i18n/en.yaml:0
msgid ""
"Plasma Mobile is an open-source user interface for phones. Running on top of "
"a Linux distribution, Plasma Mobile turns your phone into a fully hackable "
"device."
msgstr ""
"Plasma 모바일은 휴대폰용 오픈소스 사용자 인터페이스입니다. Plasma 모바일을 리"
"눅스 배포판 위에서 실행하면 휴대폰을 전문가급 장치로 바꿀 수 있습니다."

#: i18n/en.yaml:0
msgid "Contribute"
msgstr "기여"

#: i18n/en.yaml:0
msgid ""
"Using the multi-platform toolkit Qt, the flexible extensions of KDE "
"Frameworks plus the power of Plasma Shell, Plasma Mobile is built with "
"technology which feels equally at home on the desktop and mobile devices."
msgstr ""
"크로스 플랫폼 툴킷 Qt, KDE 프레임워크의 유연한 확장과 Plasma 셸의 확장성을 포"
"함하는 Plasma 모바일은 데스크톱과 모바일 장치 둘 다를 지원하는 기술로 설계되"
"었습니다."

#: i18n/en.yaml:0
msgid "KWin and Wayland"
msgstr "KWin과 Wayland"

#: i18n/en.yaml:0
msgid ""
"Wayland is the next-generation protocol for delivering cutting-edge "
"interfaces which are silky smooth. KWin is the battle-tested window manager "
"which implements Wayland, delivering a polished and reliable experience on "
"both the desktop and mobile devices."
msgstr ""
"Wayland는 부드러운 사용자 인터페이스를 지원하는 차세대 프로토콜입니다. KWin"
"은 Wayland를 구현하는 다기능 창 관리자로, 데스크톱과 모바일 장치 모두에서 세"
"련되고 안정적인 사용자 경험을 제공합니다."

#: i18n/en.yaml:0
msgid "Making full use of Open Source"
msgstr "오픈 소스의 적극적인 사용"

#: i18n/en.yaml:0
msgid ""
"Plasma Mobile combines many powerful software tools from established "
"projects to make a whole greater than the sum of its parts.<br /> "
"ModemManager provides our telephone solution and Pulseaudio drives the "
"Plasma Mobile sound system."
msgstr ""
"Plasma 모바일은 기존 프로젝트의 강력한 소프트웨어 도구를 사용하여 부가 가치"
"를 창출합니다.<br />통화 기능은 ModemManager, Plasma 모바일의 소리 시스템은 "
"PulseAudio를 사용합니다."

#: i18n/en.yaml:0
msgid "Adapting to your needs"
msgstr "내 필요에 맞추기"

#: i18n/en.yaml:0
msgid ""
"Being based on the most flexible desktop in the world means you can truly "
"make your phone your own. Add and modify widgets, change colour schemes, "
"fonts, and much, much more."
msgstr ""
"가장 유연한 데스크톱 환경을 기반으로 하고 있다는 것은 내 휴대폰을 내가 정말"
"로 원하는 대로 꾸밀 수 있다는 것을 의미합니다. 위젯을 추가 및 삭제, 색 구성표"
"나 글꼴 변경 외에도 더욱 많은 것을 할 수 있습니다."

#: i18n/en.yaml:0
msgid "Applications"
msgstr "앱"

#: i18n/en.yaml:0
msgid ""
"Plasma Mobile apps are developed with [Kirigami](https://develop.kde.org/"
"frameworks/kirigami) but applications developed for other Linux operating "
"system are generally compatible."
msgstr ""
"Plasma Mobile 앱은 [Kirigami](https://develop.kde.org/frameworks/kirigami)를 "
"사용하여 개발되지만 다른 리눅스용 앱도 호환됩니다."

#: i18n/en.yaml:0
msgid "Why?"
msgstr "왜 사용하나요?"

#: i18n/en.yaml:0
msgid ""
"The most common offerings on mobile devices lack openness and trust. In a "
"world of walled gardens, we want to create a platform that respects and "
"protects the user’s privacy to the fullest. We want to provide a fully open "
"base which others can help develop and use for themselves, or in their "
"products."
msgstr ""
"모바일 장치에 설치된 대부분 운영 체제는 개방성과 신뢰를 알 수 없습니다. 담장"
"으로 둘러싸인 세계에서 사용자의 프라이버시를 존중하고 최대한으로 보호하는 플"
"랫폼을 개발하고 있습니다. 다른 사람들이 개발을 돕고 사용할 수 있는 완전한 개"
"방된 기반 플랫폼을 제공합니다."

#~ msgid "Manjaro ARM"
#~ msgstr "Manjaro ARM"

#~ msgid "![](/img/manjaro.svg)"
#~ msgstr "![](/img/manjaro.svg)"

#~ msgid ""
#~ "Manjaro ARM is the Manjaro distribution, but for ARM devices. It's based "
#~ "on Arch Linux ARM, combined with Manjaro tools, themes and infrastructure "
#~ "to make install images for your ARM device."
#~ msgstr ""
#~ "Manjaro ARM은 Arm 장치용 Manjaro 배포판입니다. Arch Linux ARM을 기반으로 "
#~ "하며, Manjaro에서 제공하는 도구, 테마, 기반을 사용하여 Arm 장치에 설치할 "
#~ "수 있는 이미지를 만듭니다."

#~ msgid ""
#~ "[Website](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)"
#~ msgstr ""
#~ "[웹사이트](https://manjaro.org) [포럼](https://forum.manjaro.org/c/arm/)"

#~ msgid ""
#~ "[Latest Stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-"
#~ "mobile/releases)"
#~ msgstr ""
#~ "[최종 안정판(PinePhone)](https://github.com/manjaro-pinephone/plasma-"
#~ "mobile/releases)"

#~ msgid ""
#~ "[Developer builds (Pinephone)](https://github.com/manjaro-pinephone/"
#~ "plasma-mobile-dev/releases)"
#~ msgstr ""
#~ "[개발자 빌드(Pinephone)](https://github.com/manjaro-pinephone/plasma-"
#~ "mobile-dev/releases)"

#~ msgid "Installation"
#~ msgstr "설치"

#~ msgid ""
#~ "For the PinePhone, you can find generic information on [Pine64 wiki]"
#~ "(https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)."
#~ msgstr ""
#~ "PinePhone 사용자라면 [Pine64 위키](https://wiki.pine64.org/index.php/"
#~ "PinePhone_Installation_Instructions)에서 정보를 확인할 수 있습니다."

#, fuzzy
#~| msgid "Manjaro ARM"
#~ msgid "Manjaro"
#~ msgstr "Manjaro ARM"

#, fuzzy
#~| msgid ""
#~| "[Latest Stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-"
#~| "mobile/releases)"
#~ msgid ""
#~ "[Images](https://github.com/manjaro/manjaro-plasma-mobile-x64/releases)"
#~ msgstr ""
#~ "[최종 안정판(PinePhone)](https://github.com/manjaro-pinephone/plasma-"
#~ "mobile/releases)"

#~ msgid "Neon based amd64 ISO image"
#~ msgstr "Neon 기반 amd64 ISO 이미지"

#~ msgid "KWeather, Plasma mobile weather application"
#~ msgstr "KWeather, Plasma 모바일 날씨 프로그램"

#~ msgid "Kalk, a calculator application"
#~ msgstr "Kalk, 계산기 프로그램"

#~ msgid "Calindori, a calendar application"
#~ msgstr "Calindori, 캘린더 프로그램"

#~ msgid "Buho, a note taking application"
#~ msgstr "Buho, 메모 작성 프로그램"

#~ msgid "Kongress"
#~ msgstr "Kongress"

#~ msgid "Okular Mobile, a universal document viewer"
#~ msgstr "Okular 모바일, 만능 문서 뷰어"

#~ msgid "Nota, a text editor"
#~ msgstr "Nota, 텍스트 편집기"

#~ msgid "Pix, another image viewer"
#~ msgstr "Pix, 그림 뷰어"

#~ msgid "Task Board"
#~ msgstr "작업 보드"

#~ msgid "Frequently Asked Questions"
#~ msgstr "자주 묻는 질문"

#~ msgid "FAQ"
#~ msgstr "자주 묻는 질문"

#~ msgid "Why is Plasma Mobile not using Mer/Nemomobile?"
#~ msgstr "왜 Plasma 모바일은 Mer/Nemomobile을 사용하지 않나요?"

#~ msgid ""
#~ "Plasma Mobile is a software platform for mobile devices. It is not an "
#~ "operating system in itself, it consists of Qt5, the KDE Frameworks, "
#~ "Plasma and various software that's part of the application set. Plasma "
#~ "Mobile can work on top of the Mer distribution, but due to a lack of time "
#~ "and resources, we're currently focusing on KDE Neon on PinePhone as a "
#~ "base for testing and development."
#~ msgstr ""
#~ "Plasma 모바일은 모바일 장치용 소프트웨어 플랫폼입니다. 그 자체적으로 운영 "
#~ "체제를 구성하지 않으며, Qt5, KDE 프레임워크, Plasma 및 기타 프로그램으로 "
#~ "구성되어 있습니다. Plasma 모바일은 Mer 배포판에서 작동이 가능하지만 시간"
#~ "과 자원의 부족으로 인하여 PinePhone 상의 KDE Neon에서 개발과 시험을 진행하"
#~ "고 있습니다."

#~ msgid "Can Android apps work on Plasma Mobile?"
#~ msgstr "Plasma 모바일에서 안드로이드 앱을 사용할 수 있나요?"

#~ msgid ""
#~ "There are projects like [Anbox](https://anbox.io/) which is Android "
#~ "running inside Linux container, and use Linux kernel to execute "
#~ "applications to achieve near-native performance. This could be leveraged "
#~ "in the future to have Android apps running on top of a GNU/Linux system "
#~ "with the Plasma Mobile platform, but it's a complicated task, and as of "
#~ "*today*(September 6th, 2020) some distributions already support Anbox and "
#~ "you can run Plasma Mobile on top of those distributions."
#~ msgstr ""
#~ "리눅스 컨테이너에서 안드로이드를 실행시킬 수 있는 [Anbox](https://anbox."
#~ "io)가 있습니다. 리눅스 커널을 사용하므로 프로그램을 네이티브 성능에 가깝"
#~ "게 구동할 수 있습니다. 차후에 GNU/리눅스 시스템 및 Plasma 모바일 플랫폼에"
#~ "서 안드로이드 앱을 실행시키는 데 이 프로젝트의 자원을 활용할 수는 있으나, "
#~ "이 작업은 복잡합니다. 2020년 9월 6일 기준 일부 배포판에서는 Anbox를 지원하"
#~ "며 해당 배포판에서 Plasma 모바일을 구동할 수 있습니다."

#~ msgid "Can I run Plasma Mobile on my mobile device?"
#~ msgstr "내 장치에서 Plasma 모바일을 사용할 수 있나요?"

#~ msgid "Currently, Plasma Mobile runs on the following device types:"
#~ msgstr "현재 다음 장치에서 Plasma 모바일을 사용할 수 있습니다:"

#~ msgid ""
#~ "**(Recommended) PinePhone:** We offer official images built for the "
#~ "PinePhone on top of KDE Neon. You can find more information on the Plasma "
#~ "Mobile [documentation](https://docs.plasma-mobile.org)."
#~ msgstr ""
#~ "**(추천)PinePhone:** KDE Neon 기반 PinePhone용 공식 이미지를 제공합니다. "
#~ "Plasma 모바일 [문서](https://docs.plasma-mobile.org)에서 자세히 알아 보십"
#~ "시오."

#~ msgid ""
#~ "**x86-based:** If you want to try out Plasma Mobile on an Intel tablet, "
#~ "desktop/laptop, or virtual machine, the x86_64 Neon-based Plasma Mobile "
#~ "[image](https://www.plasma-mobile.org/get/) is for you. Information on "
#~ "how to permanently install it can be found on the Plasma Mobile "
#~ "[documentation](https://docs.plasma-mobile.org)."
#~ msgstr ""
#~ "**x86 기반:** Plasma 모바일을 인텔/AMD 태블릿, 데스크톱, 노트북에서 시험"
#~ "해 보려면 x86_64 Neon 기반 Plasma 모바일 [이미지](https://www.plasma-"
#~ "mobile.org/get/)를 사용하십시오. 장치에 설치할 수 있는 정보는 Plasma 모바"
#~ "일 [문서](https://docs.plasma-mobile.org)를 참조하십시오."

#~ msgid ""
#~ "**postmarketOS devices:** postmarketOS is a distribution based on Alpine "
#~ "Linux that can be installed on Android smartphones and other mobile "
#~ "devices. This project offers support for a fairly wide range of devices, "
#~ "and it offers Plasma Mobile as an available interface. Please find your "
#~ "device from the [list of supported devices]( https://wiki.postmarketos."
#~ "org/wiki/Devices) and see what's working, then you can follow the [pmOS "
#~ "installation guide]( https://wiki.postmarketos.org/wiki/"
#~ "Installation_guide) to install it on your device. Your mileage may vary "
#~ "depending on the device in use and when you use a device in the testing "
#~ "category it is **not** necessarily representative of the current state of "
#~ "Plasma Mobile."
#~ msgstr ""
#~ "**postmarketOS 장치:** postmarketOS는 안드로이드 스마트폰과 기타 모바일 장"
#~ "치에 설치할 수 있는 Alpine 리눅스 기반 배포판입니다. 이 프로젝트는 Plasma "
#~ "모바일을 사용자 인터페이스로 지원합니다. postmarketOS가 [지원하는 장치 목"
#~ "록](https://wiki.postmarketos.org/wiki/Devices)에서 사용 중인 장치 지원 여"
#~ "부와 작동하는 구성 요소를 확인했다면 [pmOS 설치 가이드](https://wiki."
#~ "postmarketos.org/wiki/Installation_guide)를 참조하여 장치에 설치하십시오. "
#~ "작동 여부는 장치에 따라 다르며, 테스트 대상 장치가 아니라면 Plasma 모바일"
#~ "의 현재 상태를 정확하게 반영하지 **못할** 수도 있습니다."

#~ msgid ""
#~ "**Other:** Unfortunately support for Halium based devices had to be "
#~ "dropped recently (see [technical debt](/2020/12/14/plasma-mobile-"
#~ "technical-debt/)). This includes the previous reference device Nexus 5x."
#~ msgstr ""
#~ "**기타:** Halium 기반 장치 지원은 최근에 삭제되었습니다. 자세한 정보는 [기"
#~ "술 부채](/2020/12/14/plasma-mobile-technical-debt)를 참조하십시오. 이전의 "
#~ "레퍼런스 장치였던 Nexus 5X도 해당합니다."

#~ msgid "I've installed Plasma Mobile, what is the login password?"
#~ msgstr "Plasma 모바일을 설치했는데 로그인 암호가 무엇인가요?"

#~ msgid ""
#~ "If you've installed Neon onto your PinePhone via the installation script, "
#~ "the password should be \"1234\", and you can then change it afterwards by "
#~ "running \"passwd\" in Konsole. For Manjaro it is 123456. When changing "
#~ "it, please keep in mind that you can currently only enter numbers on the "
#~ "lock screen."
#~ msgstr ""
#~ "설치 스크립트로 PinePhone에 Neon을 설치했다면 암호는 \"1234\"입니다. "
#~ "Konsole에서 \"passwd\" 명령을 실행하여 바꿀 수 있습니다. Manjaro의 경우에"
#~ "는 \"123456\"입니다. 암호를 변경할 때에는 현재 잠금 화면에서 숫자만 입력"
#~ "할 수 있음을 유의하십시오."

#~ msgid ""
#~ "If you're using the x86 image, no password is set by default, and you'll "
#~ "have to set it by running \"passwd\" in Konsole before you can "
#~ "authenticate for anything."
#~ msgstr ""
#~ "x86 이미지를 사용한다면 암호가 설정되어 있지 않으며, 인증해야 하는 명령어"
#~ "를 실행하기 전에 Konsole에서 \"passwd\" 명령을 실행하여 암호를 설정해야 합"
#~ "니다."

#~ msgid "What's the state of the project?"
#~ msgstr "현재 프로젝트 상태는 어떤가요?"

#~ msgid ""
#~ "Plasma Mobile is currently under heavy development and is not intended to "
#~ "be used as a daily driver. If you are interested in contributing, [join](/"
#~ "findyourway) the game."
#~ msgstr ""
#~ "Plasma 모바일은 현재 활발하게 개발 중이며 일상 사용 용도로는 적합하지 않습"
#~ "니다. 기여에 관심이 있으시다면 [역할](/findyourway)을 알아 보십시오."

#~ msgid "Join Plasma Mobile"
#~ msgstr "Plasma 모바일에 참여하기"

#~ msgid ""
#~ "[![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/"
#~ "#kde-plasmamobile)"
#~ msgstr ""
#~ "[![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/"
#~ "#kde-plasmamobile)"

#~ msgid ""
#~ "[![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/"
#~ "nextclient/chat.freenode.net/#plasma)"
#~ msgstr ""
#~ "[![](/img/irc.png)Freenode(IRC)의 #plasma 채널](https://kiwiirc.com/"
#~ "nextclient/chat.freenode.net/#plasma)"

#~ msgid "[![](/img/telegram.svg)Halium on Telegram](https://t.me/Halium)"
#~ msgstr "[![](/img/telegram.svg)텔레그램의 Halium](https://t.me/Halium)"

#~ msgid "Our Vision"
#~ msgstr "우리의 비전"

#~ msgid "Vision"
#~ msgstr "비전"

#~ msgid ""
#~ "Plasma Mobile aims to become a complete and open software system for "
#~ "mobile devices.<br /> It is designed to give privacy-aware users back "
#~ "control over their information and communication."
#~ msgstr ""
#~ "Plasma 모바일의 목표는 모바일 장치를 위한 완전한 오픈 소스 시스템 개발입니"
#~ "다.<br />프라이버시를 중시하는 사용자가 개인 정보와 통신의 통제권을 되찾"
#~ "을 수 있도록 돕습니다."

#~ msgid ""
#~ "Plasma Mobile takes a pragmatic approach and is inclusive to 3rd party "
#~ "software, allowing the user to choose which applications and services to "
#~ "use, while providing a seamless experience across multiple devices.<br /> "
#~ "Plasma Mobile implements open standards and is developed in a transparent "
#~ "process that is open for anyone to participate in."
#~ msgstr ""
#~ "Plasma 모바일은 실용적인 접근을 택하고 제3자 소프트웨어에 개방적입니다. 사"
#~ "용자는 자기가 사용할 프로그램과 서비스를 원하는 대로 결정할 수 있으며, 여"
#~ "러 장치에서 통합적으로 지원합니다.<br />Plasma 모바일은 공개 표준을 구현하"
#~ "며 누구나 참여할 수 있도록 투명하게 개발됩니다."

#~ msgid "What is Plasma Mobile?"
#~ msgstr "Plasma 모바일은 무엇인가요?"

#~ msgid ""
#~ "Plasma Mobile offers a free (as in freedom and beer), user-friendly, "
#~ "privacy-enabling and customizable platform for mobile devices. It is "
#~ "shipped by different distributions (ex. postmarketOS, Manjaro, openSUSE), "
#~ "and can run on the devices that are supported by the distribution. Most "
#~ "of the testing is done on the PinePhone, as it is easier for developers "
#~ "to obtain and develop on. However, depending on your distribution, you "
#~ "may be able to install it on your device!"
#~ msgstr ""
#~ "Plasma Mobile은 모바일 장치의 무료, 자유, 사용자 친화적, 프라이버시 존중, "
#~ "사용자 정의 가능한 플랫폼입니다. postmarketOS, Manjaro, openSUSE와 같은 여"
#~ "러 배포판에서 제공하며, 배포판에서 지원하는 장치에 설치할 수 있습니다. 대"
#~ "부분 시험은 쉽게 구할 수 있고 개발할 수 있는 PinePhone을 사용합니다. 배포"
#~ "판에 따라서 다른 장치에도 설치할 수 있습니다!"

#~ msgid ""
#~ "[Latest Edge (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-"
#~ "pinephone/plasma-mobile/)"
#~ msgstr ""
#~ "[일일 빌드(Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-"
#~ "pinephone/plasma-mobile/)"

#~ msgid "Download:"
#~ msgstr "다운로드:"

#~ msgid "Neon based reference rootfs"
#~ msgstr "Neon 기반 참조 rootfs"

#~ msgid ""
#~ "Image based on KDE Neon. KDE Neon itself is based upon Ubuntu 20.04 "
#~ "(focal). This image is based on the dev-unstable branch of KDE Neon, and "
#~ "always ships the latest versions of KDE frameworks, KWin and Plasma "
#~ "Mobile compiled from git master."
#~ msgstr ""
#~ "KDE Neon 기반 이미지입니다. KDE Neon은 우분투 20.04(Focal)를 기반으로 합니"
#~ "다. 이 이미지는 KDE Neon의 dev-unstable 브랜치를 기반으로 하며, 항상 KDE "
#~ "프레임워크, KWin, Plasma 모바일의 git master 버전을 제공합니다."

#~ msgid "There is no ongoing work for maintaining the images at the moment."
#~ msgstr "현재 이 이미지는 관리되지 않고 있습니다."

#~ msgid "[PinePhone](https://images.plasma-mobile.org/pinephone/)"
#~ msgstr "[PinePhone](https://images.plasma-mobile.org/pinephone/)"

#~ msgid ""
#~ "Download the image, uncompress it and flash it to a SD-card using `dd` or "
#~ "a graphical tool. The PinePhone will automatically boot from a SD-Card. "
#~ "To install to the embedded flash, please follow the instructions in the "
#~ "[Pine wiki](https://wiki.pine64.org/index.php/"
#~ "PinePhone_Installation_Instructions)."
#~ msgstr ""
#~ "이미지를 다운로드, 압축 해제한 후 `dd` 명령이나 그래픽 도구로 SD 카드에 기"
#~ "록하십시오. PinePhone은 SD 카드에서 부팅됩니다. 내장 메모리에 설치하려면 "
#~ "[Pine 위키](https://wiki.pine64.org/index.php/"
#~ "PinePhone_Installation_Instructions)의 내용을 참조하십시오."
