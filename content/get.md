---
title: Distributions offering Plasma Mobile
menu:
  main:
    weight: 4
    name: Install
scssFiles:
  - scss/get.scss
layout: get-involved
no_text: true
---

Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobile

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility. Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the device's wiki page for more information on what is working.

[Learn more](https://postmarketos.org)

#### Download

* [Well supported devices](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

#### Nightly
PostmarketOS provides a nightly repository that tracks **Plasma 6** development.

[Documentation](https://wiki.postmarketos.org/wiki/Plasma_Mobile#Nightly_repository)

---

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Download

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

---

### Debian

![](/img/debian.svg)

Debian provides a basic set of packages related to [plasma-mobile](packages.debian.org/plasma-mobile) in its repositories since release 12, codenamed _bookworm_. The packaging effort is continuing even after the release of bookworm, that means a broader set of packages is now available.

Thanks to the efforts of the developers of [Mobian](https://mobian-project.org/) project (a mobile derivative of Debian), flashable images based on Plasma mobile will be soon available, as well!

Instructions to install Plasma mobile on Debian systems can be found on [Debian wiki](https://wiki.debian.org/Mobian/Tweaks#Plasma_mobile).

---

### Kupfer

![](/img/distros/kupfer.svg)

Kupfer Linux is an Arch Linux ARM derived distribution aiming to provide tooling that makes it easier for developers to port devices to it.

#### Links

* [Website](https://kupfer.gitlab.io/)

---

### Sineware ProLinux

![](/img/distros/prolinux.png)

ProLinux is a GNU/Linux distribution with an immutable root filesystem, an A/B updating scheme, Flatpak-first app distribution model, and a unified API surface to interact with the OS.

**Plasma 6** development images are currently available, built from git.

#### Links
* [Website](https://sineware.ca/prolinux/)
* [Nightly Builds](http://cdn.sineware.ca/repo/prolinux/mobile/dev/arm64/)

---

### openSUSE

![](/img/openSUSE.svg)

openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.

#### Links
* [Website](https://en.opensuse.org/HCL:PinePhone)
* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)
* [Matrix Channel](https://matrix.to/#/#mobile:opensuse.org)

---

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

Links to testing images can be found in the channel.

---

## Desktop Devices

### Fedora

![](/img/fedora.svg)

Fedora has Plasma Mobile and related applications packaged in their repositories.

Install the [plasma-mobile](https://src.fedoraproject.org/rpms/plasma-mobile) package.

---

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

---

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

---

### KDE Neon

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
