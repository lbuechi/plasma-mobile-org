---
title: "This Month in Plasma Mobile: March 2023"
subtitle: "We continued the port to Qt6, prepared the 23.04 release and attended SCaLE in California"
SPDX-License-Identifier: CC-BY-4.0
date: 2023-03-29
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2023 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
images:
 - scale-1.jpg
aliases:
 - /2023/03/29/03/

scale:
- name: Picture taken at SCaLE
  url: scale-1.jpg
- name: Picture taken at SCaLE
  url: scale-2.jpg
- name: Picture taken at SCaLE
  url: scale-3.jpg
- name: Picture taken at SCaLE
  url: scale-4.jpg
- name: Picture taken at SCaLE
  url: scale-5.jpg
- name: Picture taken at SCaLE
  url: scale-6.jpg
- name: Picture taken at SCaLE
  url: scale-7.jpg
- name: Picture taken at SCaLE
  url: scale-8.jpg
- name: Picture taken at SCaLE
  url: scale-9.jpg
- name: Picture taken at SCaLE
  url: scale-10.jpg
- name: Picture taken at SCaLE
  url: scale-11.jpg

---

We have been busy these past two months!

The last version of Plasma 5 was released in February, and so development work has shifted toward Plasma 6. This involves porting from Qt5 and KDE Frameworks 5 to Qt6 and KDE Frameworks 6. As a result, development is quite chaotic right now, but we are hard at work!

There is no set timeline for when the first version of Plasma 6 will come out, but it is expected to be within a year.

For applications, they are now part of the regular KDE Gear releases and will have a release next month (KDE Gear 23.04).

In other news, we (and the rest of KDE) have a new [forum](https://discuss.kde.org/c/development/plasma-mobile/11)! General discussion and support can now take place there, which will provide an alternative to the current matrix room and mailing list.

Devin also attended [SCaLE 20x](https://www.socallinuxexpo.org/scale/20x) in California, and was able to demo a bunch of devices running Plasma Mobile!

{{< screenshots name="scale" >}}

## Plasma

Refactoring and major changes are being done in order to improve the architecture of the shell for Plasma 6. A lot of the work is not particularly visual, so please enjoy Konqi & Katie:

<img src="/img/konqi/konqi-calling.png" alt="Konqi & Katie" width=400px/>

The [plasma-mobile](https://invent.kde.org/plasma/plasma-mobile) repository was ported to KDE Frameworks 6 and Qt 6, which now starts! (Yari Polla, Devin Lin, Nicolas Fella, and many more, Plasma 6. [Link](https://invent.kde.org/plasma/plasma-mobile/-/commits/master))

The brightness slider in the action drawer was reworked to be much smoother for user interaction, and not jump around when sliding fast. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/95d1ec87b1376305ca97769e0de7befd5fa510e8))

A new system process now manages Plasma Mobile specific configurations, so that KWin configuration does not need to be installed on the system, causing issues with a co-existing Plasma Desktop install. This will largely remove the need for the [plasma-phone-settings](https://invent.kde.org/plasma-mobile/plasma-phone-settings) repository. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/d2b5416513992ca63865d75a9217e21ca2ce3599))

Window decorations now are turned on when a window is moved to an external display, and is no longer automatically fullscreen. This is the first step toward convergence support. (Plata Hill, Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/merge_requests/282))

The task switcher was rewritten and ported to be a KWin effect, similar to Overview on desktop. This offloads the rendering of task previews to KWin rather than plasmashell, which should improve the speed at which they load. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/eb03fe8c940e5922a3b99f2b666fee9c085e847b))

Mobile related settings modules were all imported into the [plasma-mobile](https://invent.kde.org/plasma/plasma-mobile) repository in order to centralize and ensure they are well-maintained. (Devin Lin, Plasma 6, [Link 1](https://invent.kde.org/plasma/plasma-mobile/-/commit/e147f98aeabf2e02865a0b387bd3595120732ba7), [Link 2](https://invent.kde.org/plasma/plasma-mobile/-/commit/1e2847ef96e441c890230d5bf1475f693c49fe2d))

The audio overlay (when pressing audio buttons) was partially rewritten and improved. The speed at which it opens from audio events should be much improved. Sound feedback when changing audio volume has been removed, which appears to have been a cause of shell crashes. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/158af43fd469b5211ad7b2a23745b57cb031b238))

From user feedback, the direction of the placeholder arrow on the Halcyon homescreen was reversed since it could be interpreted the wrong way. (Devin Lin, Plasma 5.27.1, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/64cdab12dde9212fcb554cd409a799a2b4c5f7fc))

The screen rotation quicksetting now hides if screen rotation is not available on the system. (Luca Weiss, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/f8aeffa56d5cdbb83a29b5e87842516bf640bb62))

A quicksetting was added to manage network hotspots. (Yari Polla, Plasma 6, [Link 1](https://invent.kde.org/plasma/plasma-mobile/-/merge_requests/286), [Link 2](https://invent.kde.org/plasma/plasma-mobile/-/commit/79998ab544e841ecddc42f76ad781f34585fb9ab))

The task switcher now can be opened if there are no applications running. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/eb03fe8c940e5922a3b99f2b666fee9c085e847b))

Quicksettings can now be individually disabled and enabled from the settings. (Yari Polla, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/e21354bf63c89bffb400f418cf4e3770607ab539))

The hotspot settings module was ported to modern design components. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/fa2f34ca4186cb3aa72b5ef6e8e39c6eccb974a8))

Keyboard navigation was added to the Halcyon homescreen. (Yari Polla, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/merge_requests/313))

Keyboard navigation was added to the search component on the homescreen. (Yari Polla, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/afd8f59b8975224dcf643211be513690ae2f7b64))

Internally, the mobile shell component library was refactored and split into smaller plugins, so that is much lighter to import. (Devin Lin, Plasma 6, [Link 1](https://invent.kde.org/plasma/plasma-mobile/-/commit/0775c561533d42cae692eda9be4cec6f92e90b45), [Link 2](https://invent.kde.org/plasma/plasma-mobile/-/commit/79e99a9cfe99affa61a6948ed602b9365c8d6454), [Link 3](https://invent.kde.org/plasma/plasma-mobile/-/commit/462d99b83fe8c099bf842aca153aff71e96c9294))

A mobile shell specific DBus API was added in order to facilitate communication between components. (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/f25840bfc2b89d61aa47bf79a86825574a0f3441))

The homescreen now properly animates when the screen is unlocked (Devin Lin, Plasma 6, [Link](https://invent.kde.org/plasma/plasma-mobile/-/commit/5f93d0198e4444142d64aba0fb8e4d6897dc9bdc))

And countless other bug fixes were made, too many to list here...

## Camera

There have been many developments in the broader Linux community regarding cameras!

There is currently no ongoing work by any community members on this topic. If you are interested, this could be something that you could contribute to!

## Spacebar (SMS Client)

Using Spacebar with a proxy was fixed. (Michael Lang, [Link](https://invent.kde.org/plasma-mobile/spacebar/-/merge_requests/146))

## Plasma Dialer

Notifications for missed calls have been implemented. (Marco Mattiolo, [Link](https://invent.kde.org/plasma-mobile/plasma-dialer/-/merge_requests/108))

## AudioTube (YouTube Music client)

Since the last update, we received a lot of help from our [SoK student Theophile Gilgien](https://theophile.gilgien.net/posts/blog-sok/)!

The player was redesigned to display the play queue in a sidebar or a bottom drawer (on mobile) (Mathis Brüchert)

![The new music player](audiotube-player.png)

Audiotube received a new Icon (Mathis Brüchert)

Fixes for mobile usability including a new bottom drawer component and overlay buttons to play and shuffle playlists, albums, etc. (Mathis Brüchert)

![Bottom drawer](audiotube-drawer.png)

Added about page using the Kirigami add-ons Mobile Forms
(Jonah Brüchert, Mathis Brüchert)

Search history entries can now be removed (Theophile Gilgien)

The playlist can now be cleared (Theophile Gilgien)

Fix some crashes (Jonah Brüchert)

You can now play your favorites and most played songs as a playlist (Theophile Gilgien)

Memory usage was reduced by a lot (Jonah Brüchert)

Songs can now be shared from within AudioTube (Mathis Brüchert)

Keyboard navigation was improved in the player and the search (Theophile Gilgien, Mathis Brüchert)

You can now create your own playlists (Jonah Brüchert, Mathis Brüchert)

Audiotube finally received MPRIS integration (Theophile Gilgien)

![Audiotube MPRIS integration](audiotube-playlists-MPRIS.png)

The Buttons in the player now display tooltips (Theophile Gilgien)

## Plasma Phonebook

The add contact page and the contact details now utilize mobile forms (Mathis Brüchert)

![Add contact page](phonebook-add-contact.png)

![Contact viewer](phonebook-viewer.png)

A new design for the overlay action buttons was introduced (Mathis Brüchert)

![contact page overlay](phonebook-drawer.png)

The search bar is now displayed in the application header instead of below (Mathis Brüchert)

Overflow actions are now displayed in a more mobile friendly bottom drawer (Mathis Brüchert)

![contact list](phonebook-contacts-list.png)

## Arianna (eBook reader)

Arianna now has settings (Šimon Rataj. Arianna 1.0, [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/21)) and they use the new MobileForm components (Carl Schwan, Arianna 1.0, [Link](https://invent.kde.org/graphics/arianna/-/commit/937e69ee62bcc24e9469274e0f79dec66ed8e639))

![Arianna settings](arianna-settings.png)

Arianna now compiles with Qt6 and Windows (Carl Schwan. Arianna 1.0, [Link 1](https://invent.kde.org/graphics/arianna/-/merge_requests/16), [Link 2](https://invent.kde.org/graphics/arianna/-/commit/8cfb4a137d31e13429ba42654063b409936a4467))

Prevent excessive stacking of pages (Šimon Rataj. Arianna 1.0, [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/17))

Fix adding new files to the library repeatedly (Šimon Rataj. Arianna 1.0, [Link](https://invent.kde.org/graphics/arianna/-/merge_requests/18))

You can now open a file directly with Arianna from a file manager or from the CLI (Carl Schwan. Arianna 1.0, [Link](https://invent.kde.org/graphics/arianna/-/commit/97f7a16fc8c8859b1ff8f5aa1f244cad1c8d5d20))

And generally a lot of cleanup of the code in preparation of the first release.

## Tokodon (Mastodon client)

You can now configure a proxy, before logging into your Mastodon account. (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/commit/5f0060286d8cfa0efd8c24c2f44d5a15ad5e349d))

Tokodon now can handle `web+ap://` links referencing a ActivityPub resources. (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/commit/de9b21778eafaf9c9ca32d6b1c248255574aea97))

Tokodon message composer now has a language selector where you can select which language the message is written (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/commit/091fa030d55d14da27c1900d12745197a02825cf))

Tokodon message composer will preview the message you are replying to (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/174))

Tokodon notifications can now be filtered by various predefined filters (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/184))

Tokodon now can send polls! (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/152))

![Tokodon polls](tokodon-poll.png)

On Mobile, Tokodon will now use a separate page for searching (Carl Schwan, Tokodon 24.04.0, [Link](https://invent.kde.org/network/tokodon/-/commit/fb10fcf29689aaad4d73d821bb8939d32ac3c1c7)) and will generally better communicate the current search status (e.g. currently loading, no result found, etc.) (Carl Schwan, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/commit/8baeb85ac91ee10745c481e164cc229c7dc015b8))

![Tokodon search on mobile](tokodon-search-mobile.png)

The error handling in case of connection issues or other server side errors is now better indicated to the user (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/173))

Tokodon now handles follow requests. All incoming follow requests are now displayed on a special page, so that you can accept or deny them. (Joshua Goins, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/175))

The video player introduced in the last update, got some video improvments. (Shantanu Tushar, Tokodon 23.04.0, [Link](https://invent.kde.org/network/tokodon/-/merge_requests/180))

## NeoChat (Matrix client)

States events now get folded automatically to provide a more compact layout (James Graham, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/730))

![NeoChat foldable event](neochat-foldable-event.png)

Editing messages is now inline. (James Graham, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/793))

![NeoChat inline editing](neochat-inline-edit.png)

We added a new command `/knock <room-id>` to send a knock event to a room. (Shooting Star, 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/790))

Fixed <kbd>Ctrl</kbd> + <kbd>PgUp</kbd>/<kbd>PgDn</kbd> shortcut for room switching (Kevin Wolf, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/807))

Keyboard navigation was overhauled (Kevin Wolf, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/813))

A warning is now shown when a file is too large to download (Tobias Fella, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/799))

Messages that are currently being sent are now marked as such (Tobias Fella, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/811))

![NeoChat sending message](neochat-sending.png)

The notification count is now properly refreshed when the unread stats change (Joshua Goins, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/810))

The hamburger menu was replaced by a simpler menu which works better when collapsing the room list. (James Graham, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/821))

![NeoChat wide room list](neochat-menu.png)

![NeoChat collapsed room list](neochat-menu-collapsed.png)

When creating a room, the room will also be automatically opened (Tobias Fella, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/805))

NeoChat now has more video controls. (James Graham, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/772))

![NeoChat video controls](neochat-video-control.png)

We now close the login window after the account is loaded (Tobias Fella, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/778))

It's now possible to copy an image to the clipboard (Alessio Mattiazzi, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/826))

NeoChat doesn't open the virtual keyboard on mobile anymore when switching rooms (Carl Schwan, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/835))

NeoChat settings on mobile can now be closed without closing the entire app (Carl Schwan, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/836))

There are now settings that allow you to disable the URL preview globally or at the room level (James Graham, NeoChat 23.08.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/752))

We improved the text handling and added tests for it, making composing and editing messages much more reliable. (James Graham, NeoChat 23.04.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/808))

We are now handling map events (Tobias Fella, NeoChat 23.08.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/311))

We added a quick formatting menu in the chatbar when selecting text (James Graham, NeoChat 23.08.0, [Link](https://invent.kde.org/network/neochat/-/merge_requests/740))

![NeoChat account context menu](neochat-quickformat.png)

We added a context menu to the account switcher (James Graham, NeoChat 23.08.0, [Link](neochat-accountswitcher.png))

![NeoChat account context menu](neochat-accountswitcher.png)

## PlasmaTube (YouTube client)

Thumbnails are now rounded (Mathis Brüchert, PlasmaTube 23.04.0, [Link](https://invent.kde.org/multimedia/plasmatube/-/merge_requests/35))

The media player was completely redesigned (Mathis Brüchert, PlasmaTube 23.04.0, [Link](https://invent.kde.org/multimedia/plasmatube/-/merge_requests/36))

![Plasmatube new video player](plasmatube-player.png)

The trending and search page now have loading placeholders (Joshua Goins, PlasmaTube 23.04.0, [Link](https://invent.kde.org/multimedia/plasmatube/-/merge_requests/42))

We rewrote the login flow and account management. (Joshua Goins, PlasmaTube 23.08.0, [Link](https://invent.kde.org/multimedia/plasmatube/-/merge_requests/43))

![Plasmatube login page](plasmatube-login.png)

![Plasmatube account management](plasmatube-account.png)

## Kalendar (calendar and contact book)

The contact editor and viewer was ported to MobileForm. (Carl Schwan, Kalendar 23.04.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/315))

![Kalendar new contact editor](kalendar-contact-editor.png)

We added multiple new fields to the contact editor. The Instant Messengers (Carl Schwan, Kalendar 23.04.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/315)), the blog field (Aakarsh MJ, Kalendar 23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/320)) and the business information (Aakarsh MK, Kalendar 23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/342))

We fixed a few bugs where text field could be saved while being empty. (Laurent Montel, Kalendar 23.04.0, [Link 1](https://invent.kde.org/pim/kalendar/-/merge_requests/322), [Link 2](https://invent.kde.org/pim/kalendar/-/merge_requests/321))

We added many unit tests for the calendar utils (Joshua Goins, Kalendar  23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/325)), for the addressmodel (Anant Verma, Kalendar 23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/341)) and for the reminder model (Carl Schwan, Kalendar 23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/327))

Display a placeholder message in the command box when no search result is found (Vanshpreet S Kohli, Kalendar 23.08.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/316))

Kalendar now supports setting reminders at custom times (Vanshpreet S Kohli, Kalendar 23.04.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/313))

![Kalendar custom reminder](kalendar-custom-reminder.png)

When creating an event, we don't require putting a leading '0' for hours anymore (Vanshpreet S Kohli, Kalendar 23.04.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/312))

Fix various issue with the menubar being inconsistent in the contact view (Vanshpreet S Kohli, Kalendar 23.04.0, [Link](https://invent.kde.org/pim/kalendar/-/merge_requests/318))

## Kasts (Podcast player)

Kasts can now be closed to the system tray (Bart De Vries, Kasts 23.04.0, [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/99))

![Kasts search](kasts-tray.png)

We added a way to search for episodes in Kasts (Bart De Vries, Kasts 23.04.0, [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/102))

![Kasts search](kasts-search.png)

It's now possible to customize playback speeds (Bart De Vries, Kasts 23.04.0, [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/101))

Fix issues with too many redirects when streaming episodes (Bart De Vries, Kasts 23.04.0, [Link](https://invent.kde.org/multimedia/kasts/-/commit/eedfc28f8f64d88fa1d95bcfdc13ee491beb8055))

Kasts will now obey the global proxy settings (Bart De Vries, Kasts 23.08.0, [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/107))

Episodes can now be marked as favorite (Bart De Vries, Kasts 23.08.0, [Link](https://invent.kde.org/multimedia/kasts/-/merge_requests/108))

## Contributing

Want to help with the development of Plasma Mobile? We are ~~desperately~~ looking for new contributors, **beginners are always welcome**!

Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Even if you do not have a compatible phone or tablet, you can also help us out with application development, which can be easily done from a desktop!

View our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), and consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!

Our [issue tracker documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) also gives information on how and where to report issues.

## Donate

And finally, KDE can’t work without financial support, so [consider making a donation](http://kde.org/fundraisers/yearend2022) today! This stuff ain’t cheap and KDE e.V. has ambitious hiring goals. We can’t meet them without your generous donations!
